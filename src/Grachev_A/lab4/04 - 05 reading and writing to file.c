// read strings from file, sort in increase his lenght order & record that to file
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXL 100// max string lenght
#define MAXS 50// max strings quantity

int main()
{
	FILE *fr;// file for reading
	FILE *fw;// file for strings writing
	int i=0, j, N=0;// N-strings quantity
	char str[MAXS][MAXL]={0}, *pStr[MAXS], *buf;// pStr[MAXS]-pointers to strings, buf-var for change
	fr=fopen("the_green_book.txt", "r");
	if(fr==NULL)
	{
		puts("Can't open the file");
		return -1;
	}
	while(fgets(str[N], MAXL, fr) && N<(MAXS-1))
	{
		pStr[N]=&str[N];
		N++;
	}
	fw=fopen("result.txt", "w");
	if(N!=0)
	{
		if(N>1)
		{
			puts("Strings received, start sorting strings by increase...");
			for(i=0; i<N-1; i++)// <--- compare block --->
			{
				for(j=0; j<N-1; j++)
				{
					if(strlen(pStr[j+1])<strlen(pStr[j]))
					{
						buf=pStr[j];
						pStr[j]=pStr[j+1];
						pStr[j+1]=buf;
					}
				}
			}
			puts("Done! Now strings writed in file \"result.txt\"\n");
		}
		for(i=0; i<N; i++)
		{
			fputs(pStr[i], fw);
		}
	}
	else
	{
		puts("No strings here!");
	}
	fclose(fr);
	fclose(fw);
	return 0;
}