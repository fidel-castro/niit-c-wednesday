/*Timer bomb*/
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <math.h>
#include <time.h>

#define g 9.81

int main()
{
	float H, L;
	int t, t_boom, password;
	puts("Hello, %username%!\nLet's blow up the world! Enter the height dropped bombs (in meters): ");
	if(scanf("%f", &H)>0)
	{
		puts("Enter secret password for dropping (123): ");
		if(scanf("%d", &password)!=0 && (password==123))
		{	
			clock_t now;
			t_boom=(int)(sqrt(2*H/(float)g));
			puts("Launching...\n");
			t=0;
			while(t<t_boom)
			{
				L=H-(float)(g*t*t/2);
				printf("t=%03d c\th=%.2f m\n", t, L);
				now=clock();
				while(clock()<now+1000);
				t=t++;
			}
			putchar('\a');
			putchar('\a');
			putchar('\a');
			puts("\nBOOM!!!");
			return 0;
		}
		else
			puts("Access denied!\n");
	}
	else
		puts("Enter correct height for dropping\n");
	return 0;
}