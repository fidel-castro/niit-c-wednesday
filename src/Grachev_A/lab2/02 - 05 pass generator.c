/*Pass generator*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int count=0;
	int value[3]={0}; // 3 types of symbols: big/little letters & digits
	clock_t now;
	srand(time(0));
	puts("Hello, %username%!\nPasswords generating in progress... wait few second, please...\n");
	puts("Yours unique passwords:\n");
	while(1)
	{
		value[0]=rand()%('Z'-'A'+1)+'A';
		value[1]=rand()%('9'-'0'+1)+'0';
		value[2]=rand()%('z'-'a'+1)+'a';
		count++;
		putchar(value[rand()%('0'-'3')]);
		if(count%8==0) // counting, if last symbol in password - start writing new password
		{
			putchar('\n');
		}
		if(count%80==0) // counting, must be printed 80 symbols in all
		{
			break;
		}
		now=clock(); // checking current time for symbols randomization
		while(clock()<now+1);
	}
	return 0;
}