/*Let's calculate ASCII symbols in your string*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#define SIZE 128

int main()
{
	int qua[SIZE]={0}, k=0, i=0, j=0;
	char str[80];
	clock_t now;
	srand(time(0));
	puts("Hello, %username%! Let's calculate ASCII symbols in your string.\nEnter the string: ");
	fgets(str,80,stdin);
	puts("Calculation.... wait a second, please...");
	now=clock(); // time delay for calculation
	while(clock()<now+2000);

	for(i=0; i<=(strlen(str)-1); i++)
	{
		for(j=0; j<SIZE; j++)
		{
			if(str[i]==j)
			{
				++qua[j];
				++j;
			}
		}
	}
	puts("\n Done!\n");
	printf("Symbol - Founded\n", j, qua[j]);
	for(j=0; j<SIZE; j++)
		if(qua[j]>0)
		{
			if(j=='\n')
			{
				printf("  \\n   -  %d\n", qua[j]);
			}
			else if(j==' ')
			{
				printf(" space -  %d\n", qua[j]);
			}
			else
			{
			printf("   %c   -  %d\n", j, qua[j]);
			}
		}
	putchar('\n');
	return 0;
}