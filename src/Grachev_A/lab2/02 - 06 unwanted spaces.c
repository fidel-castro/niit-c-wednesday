/*Unwanted spaces eraser*/
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>
#include <time.h>

int main()
{
	int i=0, count=0, inWord=0;
	char str[80];
	clock_t now;
	srand(time(0));
	puts("Hello, %username%!\nLet's clean garbage in your string! Enter the any string: ");
	fgets(str,80,stdin);
	puts("Cleaning... wait a second, please...");
	now=clock();
	while(str[i])
	{
		if(inWord==0 && str[i]!=' ' && str[i]!='\n')
		{
			inWord=1;
			printf("%c", str[i]);
		}
		else if(inWord==1 && str[i]!=' ' && str[i]!='\n')
		{    
			printf("%c", str[i]);
		}
		else if(inWord==1 && (str[i]==' ' || str[i]=='\n'))
		{
			inWord=0;
			printf("%c", str[i]);
		}
		i++;
	}
	putchar('\n');
	return 0;
}