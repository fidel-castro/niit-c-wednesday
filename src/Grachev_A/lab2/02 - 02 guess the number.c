/*Guess the number game*/
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int value, shot, var;
	clock_t now;
	srand(time(0));
	value=rand()%100+1;
	puts("Hello, %username%! Guess the number from 0 to 100!");
	do
	{
		if((scanf("%d", &shot)!=0) && (shot>=0 && shot<=100))
		{
			puts((shot>value)?("Your number is greater than!"):((shot<value)?("Your number is less than"):((shot==value)?("Impossible! You did it! Congratulations!!!"):("Input error!\n"))));
		}
		else
			puts("Input error!\n");
			do
			var=getchar();
			while(var!='\n' && var!=EOF);
	}
	while(shot!=value);
	return 0;
}