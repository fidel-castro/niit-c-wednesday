// england system --> metric system
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>

int main()
{
	int f, i, var;
	float s;
	while(1)
	{
		puts("Good time of day, %username%!\nLet's translate your growth in sm.\nEnter your growth in following format: ft-in, where ft means foots, in - inches");
		if((scanf("%d-%d", &f, &i)==2) && (f>=0 && f<10) && (i>=0 && i<12))
		{	
			printf("In american system your growth: %d ft %d in", f, i);
			puts("\nMetric conversion....\n");
			s=(float)(((f*12)+i)*2.54);
			printf("Your growth in metric system: %0.1f sm\n", s);
			return 0;
		}
		else
			puts("Input error! Enter your growth again\n");
	do
		var=getchar();
	while(var!='\n' && var!=EOF);
	}
	return 0;
}