// radians<--->gradus
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#define PI 3.14159265358979323846

int main()
{
	int choice;
	char unit;
	float value, gradus, radian;
	char words1[]="Let's translate angle in ";
	char words2[][20]={"radian...\n", "gradus...\n"};
	char words3[][30]={"Angle value in radian = ", "Angle value in gradus = "};
	puts("Hello, %username%!\nEnter your angle in following format:\n 45.00D - for radian or 45.00R for gradus");
	if((scanf("%f%c", &value, &unit)==2) && (unit=='D' || unit=='R') && (value>=0 && value<=360))
	{
		switch(unit)
		{
			case 'R':
				choice=0;
				radian=(float)(value*PI/180);
				break;
			case 'D':
				choice=1;
				gradus=(float)(value+180/PI);
				break;
		}
		(unit=='R')?(printf("%s%s\n%s%f\n", words1, words2[choice], words3[choice], radian)):(printf("%s%s\n%s%f\n", words1, words2[choice], words3[choice], gradus));
	}
	else
		puts("Wrong data format!\n Enter your angle in following format:\n 45.00D - for radian or 45.00R for gradus");
	return 0;
}