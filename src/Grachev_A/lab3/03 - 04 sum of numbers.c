/* Count sum of numbers in string*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LIMIT 4

int main()
{
	char i=0, j=0;
	unsigned int sum=0, number=0;
	char str[80]={0};
	puts("Hello, %username%!\nLet's calculate sum of numbers in your string.\nIf your number be more than limit (4 sign), he will be devided into groups of 4 signs.\nEnter random sequence numbers: ");
	fgets(str, 80, stdin);
	for (i=0; i<strlen(str); i++)
	{
		if(str[i]>='0' && str[i]<='9' && j<LIMIT)
		{
			number=number*10+(str[i]-'0'); // string to digit 
			j++;
		}
		else
		{
			sum=sum+number;
			number=0;
			if(j==LIMIT)
				i--;
			j=0;
		}
	}
	printf("Sum of your numbers: %d \n", sum);
	return 0;
}