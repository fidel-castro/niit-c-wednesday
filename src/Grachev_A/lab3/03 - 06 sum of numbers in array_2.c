/* Create array with lenght N and sum elements between max & min elements */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#define N 10

int main()
{
	int num[N]={0};
	int i=0, j=0, sum=0, min=99, max=num[0], min_index=0, max_index=0;
	srand(time(0));
	for(i=0; i<N; i++)
	{
		num[i]=rand()%100;
		printf("%d\t", num[i]);
		if(num[i]<min)
		{
			min=num[i];
			min_index=i;
		}
		if(num[i]>max)
		{
			max=num[i];
			max_index=i;
		}
	}
	printf("\nMin: %d  \nMax: %d\n", min, max);
	i=min_index;
	j=max_index;
	if(i>j)
	{
		for(i=max_index+1; i<min_index; i++)
		{
			sum=sum+num[i];
		}
	}
	else
	{
		for(j=min_index+1; j<max_index; j++)
		{
			sum=sum+num[j];
		}
	}
	printf("Sum: %d\n", sum);
	return 0;
}