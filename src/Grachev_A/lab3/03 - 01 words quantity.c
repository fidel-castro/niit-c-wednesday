/* words quantity */
#include <stdio.h>
#include <string.h>

int main()
{
	int i=0, count=0, dcount=0, inWord=0, isDigit=0;
	char str[80];
	puts("Hello, %username%! Enter the string: ");
	fgets(str,80,stdin);
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]=0;
	while(str[i])
	{
		if(str[i]!=' ' && inWord==0)
		{
			count++;
			inWord=1;
			if(str[i]>='0' && str[i]<='9')
				isDigit=1;
		}
		else if(str[i]!=' ' && inWord==1)
		{    
			if(str[i]>='0' && str[i]<='9')
				isDigit=1;
		}
		else if(str[i]==' ' && inWord==1)
			inWord=0;
		if(isDigit==1)
		{
			dcount++;
			isDigit=0;
		}
		i++;
	}
	printf("\nWords: %d/ Digits: %d\n",count, dcount);
	return 0;
}