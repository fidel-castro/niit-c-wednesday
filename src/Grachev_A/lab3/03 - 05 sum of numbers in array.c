/* Create array with lenght N and sum him elements between 1st negative & last positive */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#define N 10

int main()
{
	int value, i, j=N, sum=0;
	int num[N]={0};
	srand(time(0));
	for(i=0; i<N; i++)
	{
		num[i]=50-rand()%100;  // creating positive & negative elem
		printf("%d\t", num[i]);
	}
	printf("\n");
	i=0;
	while(num[i]>=0 && i<N)
	{	
		i++;
	}
	while(num[j]<=0 && j>0)
	{
		j--;
	}
	i++;
	for(i; i<j; i++)
	{
		sum=sum+num[i];
	}
	printf("Sum is %d\n", sum);
	return 0;
}